
FROM openjdk:11

RUN mkdir /app

ADD build/libs/crime-rate-service-0.0.1-SNAPSHOT.jar app/crime-rate-service.jar

ENTRYPOINT ["java","-jar","/app/crime-rate-service.jar"]
