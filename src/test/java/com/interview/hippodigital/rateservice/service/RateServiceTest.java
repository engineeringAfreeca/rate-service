package com.interview.hippodigital.rateservice.service;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = RateService.class) // make sure only beans related with this controller are scanned
public class RateServiceTest {


}
