package com.interview.hippodigital.rateservice.controller;

import com.interview.hippodigital.rateservice.dto.CrimeRateResponse;
import com.interview.hippodigital.rateservice.exception.NotFoundException;
import com.interview.hippodigital.rateservice.model.Crime;
import com.interview.hippodigital.rateservice.service.RateService;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = RateController.class) // make sure only beans related with this controller are scanned
public class RateControllerTests {

        @MockBean
        private RateService rateService;

        @Autowired
        private MockMvc mockMvc;

        List<Crime> crimeList;

        @Before
        public void init() {
                Crime crime1 = new Crime("category", "location_type", "context", "id", "month");
                Crime crime2 = new Crime("category2", "location_type2", "context2", "id2", "month2");
                crimeList = new ArrayList<>();
                crimeList.add(crime1);
                crimeList.add(crime2);
        }

        @Test
        public void getCrimeRateSuccessful() throws Exception {
                CrimeRateResponse crimeRateResponse = CrimeRateResponse.builder()
                        .country("GB")
                        .latitude(23.23322)
                        .longitude(22.1111)
                        .month("2020-02")
                        .region("West London")
                        .crimes(crimeList)
                        .build();

                when(rateService.getCrimeRate("E34RL", "2020-02")).thenReturn(crimeRateResponse);

                mockMvc.perform(get("/api/rate-crime/{postCode}/{date}" ,"E34RL", "2020-02")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{}"))
                    .andExpect(status().isOk());
        }

        @Test
        public void getCrimeRateNotFound() throws Exception {
                when(rateService.getCrimeRate("134RL", "2020-02")).thenThrow(NotFoundException.class);

                mockMvc.perform(get("/api/rate-crime/{postCode}/{date}" ,"134RL", "2020-02")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}"))
                        .andExpect(status().isNotFound());
        }

        @After
        public void cleanUp() {
                crimeList.clear();
        }
}
