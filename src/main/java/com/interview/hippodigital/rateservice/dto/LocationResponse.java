package com.interview.hippodigital.rateservice.dto;

import com.interview.hippodigital.rateservice.model.Location;
import lombok.Getter;

@Getter
public class LocationResponse {
    private String status;
    private Location result;
}
