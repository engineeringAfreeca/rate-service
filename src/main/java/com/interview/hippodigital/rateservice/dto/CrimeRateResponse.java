package com.interview.hippodigital.rateservice.dto;

import com.interview.hippodigital.rateservice.model.Crime;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class CrimeRateResponse {
    private String country;
    private double longitude;
    private double latitude;
    private String region;
    private String month;
    private List<Crime> crimes;
    private String rate;
}


