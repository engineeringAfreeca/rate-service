package com.interview.hippodigital.rateservice.model;

import lombok.Getter;

@Getter
public class Location {
    private String country;
    private String nhs_ha;
    private double longitude;
    private double latitude;
    private String european_electoral_region;
    private String primary_care_trust;
    private String region;
    private String parliamentary_constituency;
    private String nuts;
}
