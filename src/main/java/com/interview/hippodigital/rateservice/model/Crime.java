package com.interview.hippodigital.rateservice.model;

import lombok.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Crime {
    private String category;
    private String location_type;
    private String context;
    private String id;
    private String month;
}
