package com.interview.hippodigital.rateservice.exceptionHandler;

import com.interview.hippodigital.rateservice.exception.InternalServiceErrorException;
import com.interview.hippodigital.rateservice.exception.NotFoundException;
import com.interview.hippodigital.rateservice.utils.Constants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.naming.ServiceUnavailableException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unused")
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{
    LocalDateTime now = LocalDateTime.now();

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {

        List<String> list = new ArrayList<>();
        for (FieldError x : ex.getBindingResult().getFieldErrors()) {
            String defaultMessage = x.getDefaultMessage();
            list.add(defaultMessage);
        }

        ExceptionMessage exceptionMessage = ExceptionMessage.builder()
                .timestamp(now.format(DateTimeFormatter.ofPattern(Constants.ERROR_DATE_PATTERN)))
                .httpStatus(status)
                .httpCode(status.value())
                .details(list)
                .build();

        return new ResponseEntity<>(exceptionMessage, headers, status);
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {

        List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " +
                    violation.getPropertyPath() + ": " + violation.getMessage());
        }

        ExceptionMessage exceptionMessage = ExceptionMessage.builder()
                .timestamp(now.format(DateTimeFormatter.ofPattern(Constants.ERROR_DATE_PATTERN)))
                .httpStatus(HttpStatus.BAD_REQUEST)
                .httpCode(HttpStatus.BAD_REQUEST.value())
                .details(errors)
                .build();


        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ExceptionMessage> handleNotFoundException(NotFoundException exception) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder()
                .timestamp(now.format(DateTimeFormatter.ofPattern(Constants.ERROR_DATE_PATTERN)))
                .message(HttpStatus.NOT_FOUND.getReasonPhrase())
                .details(Collections.singletonList(exception.getMessage()))
                .httpStatus(HttpStatus.NOT_FOUND)
                .httpCode(HttpStatus.NOT_FOUND.value())
                .build();
        logger.error(exceptionMessage);
        return new ResponseEntity<>(exceptionMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    public ResponseEntity<ExceptionMessage> handleServiceUnavailableException(ServiceUnavailableException exception) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder()
                .timestamp(now.format(DateTimeFormatter.ofPattern(Constants.ERROR_DATE_PATTERN)))
                .message(HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase())
                .details(Collections.singletonList(exception.getMessage()))
                .httpStatus(HttpStatus.SERVICE_UNAVAILABLE)
                .httpCode(HttpStatus.SERVICE_UNAVAILABLE.value())
                .build();
        logger.error(exception);
        return new ResponseEntity<>(exceptionMessage, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler( IllegalArgumentException.class)
    public ResponseEntity<ExceptionMessage> handleIllegalArgumentException(IllegalArgumentException exception) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder()
                .timestamp(now.format(DateTimeFormatter.ofPattern(Constants.ERROR_DATE_PATTERN)))
                .message(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .details(Collections.singletonList(exception.getMessage()))
                .httpStatus(HttpStatus.BAD_REQUEST)
                .httpCode(HttpStatus.BAD_REQUEST.value())
                .build();
        logger.error(exceptionMessage);
        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InternalServiceErrorException.class)
    public ResponseEntity<ExceptionMessage> handleServerErrorException(InternalServiceErrorException exception) {
        ExceptionMessage exceptionMessage = ExceptionMessage.builder()
                .timestamp(now.format(DateTimeFormatter.ofPattern(Constants.ERROR_DATE_PATTERN)))
                .message(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
                .details(Collections.singletonList(exception.getMessage()))
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .httpCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();
        logger.error(exception);
        return new ResponseEntity<>(exceptionMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
