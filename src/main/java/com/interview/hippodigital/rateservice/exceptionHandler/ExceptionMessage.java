package com.interview.hippodigital.rateservice.exceptionHandler;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Builder
@Data
public class ExceptionMessage {
    private String timestamp;
    private String message;
    private List<String> details;
    private HttpStatus httpStatus;
    private int httpCode;
}