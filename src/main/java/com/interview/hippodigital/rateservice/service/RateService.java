package com.interview.hippodigital.rateservice.service;

import com.interview.hippodigital.rateservice.dto.CrimeRateResponse;
import com.interview.hippodigital.rateservice.dto.LocationResponse;
import com.interview.hippodigital.rateservice.exception.InternalServiceErrorException;
import com.interview.hippodigital.rateservice.exception.NotFoundException;
import com.interview.hippodigital.rateservice.model.Crime;
import com.interview.hippodigital.rateservice.model.Location;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RateService {

    @Value("${services.postCodeAPI}")
    private String postCodeAPI;

    @Value("${services.policeDataAPI}")
    private String policeDataAPI;

    @Autowired
    private WebClient.Builder webClientBuilder;


    public CrimeRateResponse getCrimeRate(String postCode, String date){
        log.debug(String.format("Get crime rate for the <'%s'> post code and date <'%s'>", postCode, date));
        Location location = getLocation(postCode);
        List<Crime> crimeList = getCrimeList(date, location.getLongitude(), location.getLatitude());

        return CrimeRateResponse.builder()
                .country(location.getCountry())
                .latitude(location.getLatitude())
                .longitude(location.getLongitude())
                .month(date)
                .region(location.getRegion())
                .crimes(crimeList)
                .build();
    }

    public Location getLocation(String postCode){
        log.debug(String.format("requesting location for the <'%s'> post code", postCode));
        LocationResponse locationResponse = webClientBuilder.build()

                .get()
                .uri(postCodeAPI + "/" + postCode)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        error -> Mono.error(new NotFoundException(String.format("Location not found for <%s> post code", postCode))))
                .onStatus(HttpStatus::is5xxServerError,
                        error -> Mono.error(new InternalServiceErrorException("Something went wrong")))
                .bodyToMono(LocationResponse.class)
                .block();

        return  locationResponse.getResult();
    }

    public List<Crime> getCrimeList(String date, double longitude, double latitude){
        log.debug(String.format("requesting list of crimes for location with lat <'%f'> and lng <'%f'> and date <'%s'>", latitude, latitude, date));
        Flux<Crime> crimeFlux = webClientBuilder.build()
                .get()
                .uri(policeDataAPI + "?date=" + date + "&lat=" + latitude + "&lng=" + longitude)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        error -> Mono.error(new NotFoundException(String.format("Crime not found for date for <%s> post code and " +
                                "location with lat <'%f'> and lng <'%f'> and date <'%s'>", latitude, latitude, date))))
                .onStatus(HttpStatus::is5xxServerError,
                        error -> Mono.error(new InternalServiceErrorException("Something went wrong")))
                .bodyToFlux(Crime.class);

        return crimeFlux.collect(Collectors.toList()).share().block();
    }
}
