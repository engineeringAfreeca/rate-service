package com.interview.hippodigital.rateservice.controller;

import com.interview.hippodigital.rateservice.dto.CrimeRateResponse;
import com.interview.hippodigital.rateservice.service.RateService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@ApiResponses(value = {
        @ApiResponse(code = 400, message = "Something went wrong"),
        @ApiResponse(code = 401, message = "Unauthorised Operation"),
        @ApiResponse(code = 409, message = "Conflict with existing resource"),
        @ApiResponse(code = 403, message = "Access denied")})
public class RateController {

    @Autowired
    private RateService rateService;

    @ApiOperation(value="Retrieve crime rate")
    @GetMapping("/rate-crime/{postCode}/{date}")
    public ResponseEntity<CrimeRateResponse> getCrimeRate(@PathVariable String postCode, @PathVariable String date) {
        return new ResponseEntity<CrimeRateResponse>(rateService.getCrimeRate(postCode, date), HttpStatus.OK);
    }
}
