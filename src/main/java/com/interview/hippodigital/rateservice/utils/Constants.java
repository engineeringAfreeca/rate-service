package com.interview.hippodigital.rateservice.utils;

/*
 * final Class - prevent the class to be extended, which is a best practice for utility classes.
 * private non-argument constructor - Preventing the class from being instantiated;
 */
public final class Constants {

    private Constants() {}

    public static final String ERROR_DATE_PATTERN = "dd/MM/yyyy hh.mma";
}
